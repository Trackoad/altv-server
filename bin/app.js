const Process = require("process");

const Server = require("./modules/server");
const Compilation = require("./modules/compilation");
const UserConfig = require("../config.json");

Server.config.environement = UserConfig.environement;
Server.config.pathToServer = UserConfig.pathToServer;
Server.config.outFolder = Compilation.outFolder = UserConfig["cache-folder"];

const args = Process.argv.slice(2);
let debugMode = args[0] === "--dev";

Compilation.onCompilationEnd = (restart) => {
    if (restart) {
        Server.start();
    }
}

Server.generateConfig(debugMode);

switch (args[0]) {
    case "--prod":
        Compilation.update();
        break;
    case "--server":
        Server.start();
        break;
    default:
        Compilation.init();
        Compilation.update();
        break;
}

Process.on('SIGINT', () => Server.stop(() => { }, true));

