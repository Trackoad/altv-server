const Logger = {
    Colors: {
        FgBlack: { _id: "\x1b[30m" },
        FgRed: { _id: "\x1b[31m" },
        FgGreen: { _id: "\x1b[32m" },
        FgYellow: { _id: "\x1b[33m" },
        FgBlue: { _id: "\x1b[34m" },
        FgMagenta: { _id: "\x1b[35m" },
        FgCyan: { _id: "\x1b[36m" },
        FgWhite: { _id: "\x1b[37m" },
        FgClassic: { _id: "\x1b[0m" },
    }
};

Logger.log = (text, color = Logger.Colors.FgClassic) => {
    console.log(`${color._id}%s${Logger.Colors.FgClassic._id}`, text);
}

Logger.error = (text, throwException = true) => {
    Logger.log(text, Logger.Colors.FgRed);
    if (throwException) {
        throw new Error(text);
    }
}

Logger.warning = text => {
    Logger.log(text, Logger.Colors.FgYellow);
}

Logger.logLot = (colors, ...text) => {
    let colorResult = '';
    colors.forEach(color => colorResult += `${color._id}%s`);
    colorResult += Logger.Colors.FgClassic._id;
    console.log(colorResult, ...text);
}

module.exports = Logger;