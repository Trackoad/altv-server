
export declare class Color { }

export const Colors: {
    FgBlack: Color;
    FgRed: Color;
    FgGreen: Color;
    FgYellow: Color;
    FgBlue: Color;
    FgMagenta: Color;
    FgCyan: Color;
    FgWhite: Color;
    FgClassic: Color;
}

export function log(text: string): void;

export function log(text: string, color: Color): void;

export function error(text: string, throwException: boolean): void;

export function warning(text: string): void;

export function logLot(elements: Array<Color>, ...texts: Array<string>): void;

