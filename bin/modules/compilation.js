const watch = require('node-watch');
const ChildProcess = require("child_process");

const Logger = require("./logger");

const Compilation = {
    onCompilationEnd: () => { },
    _inited: false,
    _files: [],
    _promiseClient: null,
    _srcFolder: 'src',
    outFolder: 'cache',
    buildCommands: {
        client: "cd src\\client && npx tsc",
        server: "cd src\\server && npx tsc"
    }
}

const displayFile = (files = []) => {
    files.forEach(file => Logger.log(` - ${file.name} ${file.evt === "update" ? "✏️" : "♻️"}`, file.evt === "update" ? Logger.Colors.FgGreen : Logger.Colors.FgRed));
}

const promiseFile = (evt, name) => {
    Compilation._files.push(name);
    if (Compilation._promiseClient) {

        Compilation._promiseClient.then(() => {
            throw "";
        }).catch(() => undefined);

    }
    Compilation._promiseClient = new Promise(resolve => {
        setTimeout(() => {
            if (!Compilation._files.length) {
                resolve();
                Compilation._promiseClient = null;
                return;
            }

            const clientsFile = [];
            const serverFiles = [];
            const otherFiles = [];

            Compilation._files.forEach(file => {
                if (file.includes(`\\client\\`)) {
                    clientsFile.push({ name: file, evt: evt });
                } else if (file.includes(`\\server\\`)) {
                    serverFiles.push({ name: file, evt: evt });
                } else {
                    otherFiles.push({ name: file, evt: evt });
                }
            });

            let type = undefined;
            if (otherFiles.length) {
                Logger.log(`[Constants]`, Logger.Colors.FgMagenta);
                displayFile(otherFiles);
                type = null;
            }

            if (clientsFile.length) {
                Logger.log(`[Client]`, Logger.Colors.FgMagenta);
                displayFile(clientsFile);
                if (type !== null) {
                    type = "client";
                }
            }

            if (serverFiles.length) {
                Logger.log(`[Server]`, Logger.Colors.FgMagenta);
                displayFile(serverFiles);
                if (type !== null && !type) {
                    type = "server";
                } else if (type) {
                    type = null;
                }
            }

            Compilation.update(type);
            Compilation._files = [];
            resolve();
            Compilation._promiseClient = null;
        }, 500);
    });
}

Compilation.init = () => {
    if (Compilation._inited) {
        Logger.warning('Already init');
        return;
    }

    watch(Compilation._srcFolder, { recursive: true, filter: /\.ts$/ }, (evt, name) => promiseFile(evt, name));
}

Compilation.update = type => {
    let restart = true;
    const bClient = type === "client";
    const bServer = type === "server";
    let clientCompiled = false;

    if (!type || bClient) {
        try {
            const timer = Date.now();
            ChildProcess.execSync(`${Compilation.buildCommands.client} --outDir ../../${Compilation.outFolder}/client`);
            Logger.log(`\n[Client]: compiled ✔️`, Logger.Colors.FgYellow);
            Logger.log(`time: ${(Date.now() - timer) / 1000} seconds`, Logger.Colors.FgYellow);
            clientCompiled = true;
        } catch (err) {
            Logger.log(`\n[Compilation error]: Client ❌\n`, Logger.Colors.FgRed);
            restart = false;
        }
    }

    if (restart && (!type || bServer)) {
        try {
            const timer = Date.now();
            ChildProcess.execSync(`${Compilation.buildCommands.server} --outDir ../../${Compilation.outFolder}/server`);
            Logger.log(`${!clientCompiled ? '\n' : ''}[Server]: compiled ✔️`, Logger.Colors.FgYellow);
            Logger.log(`time: ${(Date.now() - timer) / 1000} seconds`, Logger.Colors.FgYellow);
        } catch (error) {
            Logger.log(`${!clientCompiled ? '\n' : ''}[Compilation error]: Server ❌\n`, Logger.Colors.FgRed)
            restart = false
        }
    }

    Compilation.onCompilationEnd(restart);
}

module.exports = Compilation;