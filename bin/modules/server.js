const Process = require('process');
const ChildProcess = require("child_process");
const FileSystem = require('fs');

const Logger = require("./logger");

const Server = {
    config: {
        environement: "windows",
        logFileName: 'server.log',
        titleServer: 'Alt Server - Development',
        pathToServer: undefined,
        _serverFileConfig: 'server-dev.cfg',
        intervalCheckLog: 1500,
        outFolder: 'cache'
    },
    _serverExecution: undefined,
    _watchLog: undefined,
    _logLine: 0,
    _timerAfterServerExecution: 1500
};

const setServerExecution = () => {
    if (!Server._serverExecution) {
        if (!Server.config.pathToServer) {
            Logger.error('Need parameter : config.pathToServer');
        }
        Server._serverExecution = `taskkill /FI "WindowTitle eq ${Server.config.titleServer}*" /T /F && cd "${Server.config.pathToServer}" && start /MIN "${Server.config.titleServer}" altv-server.exe --config "${Process.cwd()}\\${Server.config.outFolder}\\${Server.config._serverFileConfig}" --logfile "${Process.cwd()}\\${Server.config.outFolder}\\${Server.config.logFileName}"`;
    }
}

const startWindows = () => {

    setServerExecution();

    Server.stop(() => {
        ChildProcess.exec(Server._serverExecution, { windowsHide: false });
        setTimeout(() => {

            Logger.log("\n[Server Application]: started ⭐️\n", Logger.Colors.FgCyan);

            Server._logLine = 0;

            const display = () => {
                const data = FileSystem.readFileSync(Server.config.outFolder + "\\" + Server.config.logFileName).toString().split('\n');

                if (data) {
                    data.pop();
                    let prevError = false;
                    for (let index = Server._logLine; index < data.length; index++) {
                        let display = data[index].substring(10);
                        if (display.toLowerCase().includes('error') || (prevError && data[index].charAt(0) !== "[")) {
                            Logger.log(data[index], Logger.Colors.FgRed);
                            prevError = true;
                        } else if (display.toLowerCase().includes('warning')) {
                            Logger.log(data[index], Logger.Colors.FgYellow);
                        } else if (display.toLowerCase().includes('MiniDump created.'.toLowerCase())) {
                            Logger.log("\n[Server Application]: stopped ⭐️\n", Logger.Colors.FgCyan);
                        } else {
                            prevError = false;
                            Logger.logLot([Logger.Colors.FgBlue, Logger.Colors.FgClassic], data[index].substring(0, 10), display);
                        }
                    }
                    Server._logLine = data.length;
                }
            };
            display();

            Server._watchLog = true;
            FileSystem.watchFile(Server.config.outFolder + "\\" + Server.config.logFileName, { interval: Server.config.intervalCheckLog }, display);
        }, Server._timerAfterServerExecution);
    });
}

const startLinux = () => {
    Logger.warning('Linux is not implemented');
}

Server.start = () => {
    if (Server._watchLog) {
        FileSystem.unwatchFile(Server.config.outFolder + "\\" + Server.config.logFileName);
    }
    if (Server.config.environement === "windows") {
        startWindows();
    }
    if (Server.config.environement === "linux") {
        startLinux();
    }
}

Server.stop = (callback, message) => {
    ChildProcess.exec(`taskkill /FI "WindowTitle eq ${Server.config.titleServer}*" /T /F`, callback);
    if (message) {
        Logger.log("\n[Server Application]: stopped ⭐️\n", Logger.Colors.FgCyan);
    }
}

Server.generateConfig = (devMode) => {

    let UserConfig;

    try {
        UserConfig = require('../../config.json');
    } catch {
        Logger.error('No config file !');
    }

    let cfgData = '';
    for (const key of Object.keys(UserConfig.alt)) {
        const value = UserConfig.alt[key];
        cfgData += `${key}: ${JSON.stringify(value)}\n`;
    }
    if (devMode) {
        cfgData += `debug: true\n`;
    }
    cfgData += `modules: [ node-module ]\n`;
    cfgData += `resources: [ ${Process.cwd().split("\\").pop()} ]\n`;

    if (!FileSystem.existsSync(Server.config.outFolder)) {
        FileSystem.mkdirSync(Server.config.outFolder);
    }

    FileSystem.writeFileSync(`${Server.config.outFolder}\\${Server.config._serverFileConfig}`, cfgData);

    Logger.warning('Config server file created ✔️');

    FileSystem.writeFileSync('resource.cfg', `type: js,
main: ${Server.config.outFolder}/server/server/server.js,
client-main: ${Server.config.outFolder}/client/client/client.js,
client-files: [${Server.config.outFolder}/client/*]`)

    Logger.warning('Resource file created ✔️');

}

module.exports = Server;