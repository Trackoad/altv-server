export function start(): void;

export function stop(onStop?: (error: ExecException, stdout: string, stderr: string) => void, withMessage?: boolean): void;

export function generateConfig(devMode: boolean): void;

export let config: {
    environement: "linux" | "windows";
    logFileName: string;
    titleServer: string;
    pathToServer: string;
    outFolder: string;
};