export function init(): void;

export function update(): void;

export const onCompilationEnd: (restart: boolean) => void;

export const outFolder: string;

export const buildCommands: {
    client?: string,
    server?: string
}