# 🧰Alv:V Server

## Instructions

Install

```shell
npm i
```

Create `config.json` with this schema `config.schema.json`.

## Launch

- Build client files

```shell
npm build-client
```

- Build server files

```shell
npm build-server
```

- Compile and run

```shell
npm prod
```

- Launch dev control

```shell
npm dev
```

- Start server

```shell
npm start-server
```