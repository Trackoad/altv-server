import * as AltServer from 'alt-server';

import { Weapons } from '../constants/global/weapons';
import { PedPositionSpawn } from '../constants/server/spawn';

import { AltService } from './service/alt-service';
import { DataBaseService } from './service/database-service';
import { VehicleModel } from './model/vehicle-model';
import { EventService } from './service/event-service';

console.log = AltServer.log;

class Main {

    static start(_args?: Array<string>) {

        new EventService();

        DataBaseService.instance.getAll<VehicleModel>(VehicleModel).forEach(vehicleDb => {
            const vehicle = VehicleModel.init(vehicleDb);
            VehicleModel.vehiclesList.push(vehicle);
            vehicle.spawn();
        });
    }

}

Main.start(); 