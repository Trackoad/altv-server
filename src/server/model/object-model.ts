import * as AltServer from 'alt-server';
import { DatabaseEntityModel, DatabaseName, DatabaseIgnore } from "../service/database-service";
import { VehicleModel } from "./vehicle-model";
import { PlayerModel } from "./player-model";

@DatabaseName('object')
export class ObjectModel extends DatabaseEntityModel {

    model: number;

    position: AltServer.Vector3;

    zone: number;

    update(): { [key: string]: any; } {
        const result = {};
        return result;
    }

}