import * as AltServer from 'alt-server';
import { DatabaseEntityModel, DatabaseIgnore, DatabaseName } from '../service/database-service';
import { UtilsService } from '../service/utils-service';

@DatabaseName('player')
export class PlayerModel extends DatabaseEntityModel {

    static playersList: Array<PlayerModel> = [];

    static init(playerAny: any, playerObj: AltServer.Player): PlayerModel {
        const player = new PlayerModel();
        Object.getOwnPropertyNames(playerAny).forEach(prop => (<any>player)[prop] = playerAny[prop]);
        player._playerAlt = playerObj;
        return player;
    }

    static findPlayer(name: string): PlayerModel {
        for (const player of PlayerModel.playersList) {
            if (name === player._name) {
                return player;
            }
        }
    }

    @DatabaseIgnore()
    private _playerAlt: AltServer.Player = undefined;
    get playerAlt(): AltServer.Player {
        return this._playerAlt;
    }

    private _position: AltServer.Vector3;

    private _rotation: AltServer.Vector3;

    private _dimension = 0;

    private _life = 100;

    private _name: string;

    private _model: number;

    constructor(options?: {
        position: AltServer.Vector3,
        rotation: AltServer.Vector3,
        dimension: number,
        name: string,
        model: number,
        player: AltServer.Player
    }) {
        super();
        if (!options) {
            return;
        }
        this._position = options.position;
        this._rotation = options.rotation;
        this._dimension = options.dimension;
        this._name = options.name;
        this._model = options.model;
        this._playerAlt = options.player;
    }

    spawn() {
        this._playerAlt.spawn(this._position.x, this._position.y, this._position.z, 0);
        this._playerAlt.rot = this._rotation;
        this._playerAlt.model = this._model;
        this._playerAlt.dimension = this._dimension;
        this._playerAlt.health = this._life + 100;
    }

    revive() {

    }

    update(): { [key: string]: any } {

        const result: { [key: string]: any } = {};
        if (!UtilsService.isSame(this._position, this._playerAlt.pos)) {
            result._position = this._position = this._playerAlt.pos;
        }

        if (!UtilsService.isSame(this._rotation, this._playerAlt.rot)) {
            result._rotation = this._rotation = this._playerAlt.rot;
        }

        if ((this._playerAlt.health - 100) !== this._life) {
            result._life = this._life = this._playerAlt.health - 100;
        }

        if (this._playerAlt.dimension !== this._dimension) {
            result._dimension = this._dimension = this._playerAlt.dimension;
        }

        return result;
    }

    isConnected(): boolean {
        return this._playerAlt && this._playerAlt.valid;
    }

}