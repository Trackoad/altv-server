import * as AltServer from 'alt-server';
import { DatabaseEntityModel, DatabaseIgnore, DatabaseName } from '../service/database-service';
import { UtilsService } from '../service/utils-service';

@DatabaseName('vehicle')
export class VehicleModel extends DatabaseEntityModel {

    static vehiclesList: Array<VehicleModel> = [];

    static init(vehicleAny: any): VehicleModel {
        const vehicle = new VehicleModel();
        Object.getOwnPropertyNames(vehicleAny).forEach(prop => (<any>vehicle)[prop] = vehicleAny[prop]);
        return vehicle;
    }

    static findVehicle(vehicle: AltServer.Vehicle): VehicleModel {
        for (const vehicleObj of VehicleModel.vehiclesList) {
            if (vehicleObj._vehicleAlt && vehicleObj._vehicleAlt.id === vehicle.id) {
                return vehicleObj;
            }
        }
    }

    @DatabaseIgnore()
    private _vehicleAlt: AltServer.Vehicle = undefined;
    get vehicleAlt(): AltServer.Vehicle {
        return this._vehicleAlt;
    }

    private _position: AltServer.Vector3;

    private _rotation: AltServer.Vector3;

    private _modelVehicle: string | number;

    private _dimension: number = 0;

    constructor(options?: {
        dimension: number,
        modelVehicle: string | number,
        position: AltServer.Vector3,
        rotation: AltServer.Vector3,
    }) {
        super();
        if (!options) {
            return;
        }
        this._dimension = options.dimension;
        this._modelVehicle = options.modelVehicle;
        this._position = options.position;
        this._rotation = options.rotation;
    }

    spawn() {
        this._vehicleAlt = new AltServer.Vehicle(this._modelVehicle, this._position.x, this._position.y, this._position.z, this._rotation.x, this._rotation.y, -1 * this._rotation.z);
        this._vehicleAlt.dimension = this._dimension;
        this.update();
    }

    update(): { [key: string]: any } {
        const result: { [key: string]: any } = {};
        if (!UtilsService.isSame(this._position, this._vehicleAlt.pos)) {
            result._position = this._position = this._vehicleAlt.pos;
        }
        if (!UtilsService.isSame(this._rotation, this._vehicleAlt.rot)) {
            result._rotation = this._rotation = this._vehicleAlt.rot;
        }
        return result;
    }
}