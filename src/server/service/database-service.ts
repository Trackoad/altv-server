import DiskDb = require('diskdb');

import * as AltServer from 'alt-server';

export class DataBaseService {

    static REGEXP_DBNAME = /^[a-z]{1,20}$/;

    private getCollection(type: Function): DiskDb.CollectionDb {
        const collectionName = type.prototype._dbName;
        const collection = (<any>DiskDb)[collectionName];
        if (!collection) {
            DiskDb.loadCollections([collectionName]);
        }
        return (<any>DiskDb)[collectionName];
    }

    private static _instance: DataBaseService;
    static get instance(): DataBaseService {
        return this._instance ? this._instance : this._instance = new DataBaseService();
    }

    private constructor() {
        DiskDb.connect('./database');
    }

    insertAll<T extends DatabaseEntityModel>(value: Array<T>, type: Function) {
        this.getCollection(type).save(value);
    }

    insert<T extends DatabaseEntityModel>(value: T, type: Function) {
        const inserted = this.getCollection(type).save(this.removeIgnoreElements(value, type));
        (<any>value)._id = inserted._id;
    }

    update<T extends DatabaseEntityModel>(value: T, type: Function) {
        const updatedValue = value.update();
        if (!Object.keys(updatedValue).length) {
            return;
        }
        this.getCollection(type).update({ _id: value._id }, updatedValue);
    }

    getAll<T extends DatabaseEntityModel>(type: Function, search?: QuerySearch): Array<T> {
        return this.getCollection(type).find(search);
    }

    getOneBy(search: QuerySearch, type: Function): any {
        return this.getCollection(type).findOne(search);
    }

    private removeIgnoreElements<T>(value: T, type: Function): T {
        const ignoreList: Array<string> = type.prototype._ignoreList;
        const out = { ...value };
        (ignoreList || []).forEach(ignore => delete (<any>out)[ignore]);
        return out;
    }

}

export abstract class DatabaseEntityModel {

    readonly _id: string;

    abstract update(): { [key: string]: any };
}

export function DatabaseIgnore(): Function {
    return function (prototype: any, propertyName: string, descriptor: PropertyDescriptor) {
        if (!(prototype._ignoreList instanceof Array)) {
            prototype._ignoreList = [];
        }
        prototype._ignoreList.push(propertyName);
        return descriptor;
    };
}

export function DatabaseName(name: string): ClassDecorator {
    return function (target: Function) {
        if (!name || !name.match(DataBaseService.REGEXP_DBNAME)) {
            AltServer.logWarning(name);
            throw new Error("Bad regexp");
        }
        target.prototype._dbName = name;
    }
}