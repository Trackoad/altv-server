import * as AltServer from 'alt-server';
import { DataBaseService } from './database-service';
import { VehicleModel } from '../model/vehicle-model';
import { PlayerModel } from '../model/player-model';
import FileSystem = require('fs');
import { EventClientToServer, EventServerToClient, UpdateObjectType } from '../../constants/global/events';
import { AltService } from './alt-service';
import { Weapons } from '../../constants/global/weapons';
import { PedPositionSpawn } from '../../constants/server/spawn';
import { ObjectModel } from '../model/object-model';

export class EventService {

    private static TIMER_RENDER_SAVE = 15000;

    private static readonly COORDS_FILE = 'coords.json';

    constructor() {

        AltServer.on("playerLeftVehicle", (_, vehicle) => AltServer.setTimeout(() => DataBaseService.instance.update(VehicleModel.findVehicle(vehicle), VehicleModel), 1000));
        AltServer.on("playerConnect", player => this.playerConnected(player));
        AltServer.on("playerDisconnect", player => this.playerDisconnect(player))
        AltServer.on("playerDeath", (victim, killer, weapon) => AltServer.setTimeout(() => this.revive(victim), 5000));

        AltService.onClient(EventClientToServer.KICK_PLAYER, player => this.kickPlayer(player));
        AltService.onClient(EventClientToServer.SPAWN_VEHICLE, (player, vehicle) => this.spawnVehicle(player, vehicle, true));
        AltService.onClient(EventClientToServer.LOG, (...args) => AltServer.log("[CLIENT]", ...args));
        AltService.onClient(EventClientToServer.ERROR, AltServer.logError)
        AltService.onClient(EventClientToServer.WARNING, AltServer.logWarning)
        AltService.onClient(EventClientToServer.REVIVE, player => this.revive(player));
        AltService.onClient(EventClientToServer.CREATE_OBJECT, (player, model, pos) => this.createObject(model, pos))
        AltService.onClient(EventClientToServer.COORD, player => {
            AltServer.setTimeout(() => {
                this.savePlayerPosition(player)
            }, 100);
        });
        AltService.onClient(EventClientToServer.GIVE_WEAPON, (player, weapon = Weapons.handguns.appistol) => this.giveWeapon(player, weapon));

        AltServer.setInterval(() => this.renderSave(), EventService.TIMER_RENDER_SAVE);
    }

    private renderSave() {
        VehicleModel.vehiclesList.forEach(vehicle => {
            if (vehicle.vehicleAlt.driver) {
                DataBaseService.instance.update(vehicle, VehicleModel);
            }
        });
        PlayerModel.playersList.forEach(player => {
            if (player.isConnected()) {
                DataBaseService.instance.update(player, PlayerModel);
            }
        });
    }

    private playerConnected(player: AltServer.Player) {
        this.revive(player);
        player.dimension = player.id;
        player.rot = new AltServer.Vector3(0, 0, -50);
        player.model = 0xC8BB1E52;
        AltService.emitClient(player, EventServerToClient.DISABLE_CONTROL, false);
        player.rot = { x: 0, y: 0, z: -3.01791 };
        AltServer.log('[Connected]', player.name);
        let playerDb = DataBaseService.instance.getOneBy({ _name: player.name }, PlayerModel);
        AltServer.setTimeout(() => {
            AltService.emitClient(player, EventServerToClient.DISABLE_CONTROL, true);
            let playerObj: PlayerModel;
            if (!playerDb) {
                playerObj = new PlayerModel({
                    dimension: 0,
                    name: player.name,
                    position: PedPositionSpawn,
                    rotation: { x: 0, y: 0, z: 0 },
                    model: 1423699487,
                    player: player
                });
                DataBaseService.instance.insert(playerObj, PlayerModel);
            } else {
                playerObj = PlayerModel.init(playerDb, player);
            }
            PlayerModel.playersList.push(playerObj);
            playerObj.spawn();
            const objects: UpdateObjectType = [];
            const objsDb = DataBaseService.instance.getAll<ObjectModel>(ObjectModel);

            objsDb.forEach(obj => {
                objects.push({
                    key: 'C',
                    value: { id: obj._id, model: obj.model, pos: obj.position, zone: obj.zone }
                })
            });

            AltService.emitClient(player, EventServerToClient.UPDATE_OBJECTS, objects);
        }, 2000);
    }

    private playerDisconnect(player: AltServer.Player) {
        AltServer.log('[Disconnect]', player.name);
        const playerObj = PlayerModel.findPlayer(player.name);
        DataBaseService.instance.update(playerObj, PlayerModel);
        PlayerModel.playersList.splice(PlayerModel.playersList.indexOf(playerObj), 1);
    }

    private revive(player: AltServer.Player) {
        player.spawn(PedPositionSpawn.x, PedPositionSpawn.y, PedPositionSpawn.z, 0);
    }

    private kickPlayer(player: AltServer.Player) {
        player.kick();
    }

    private spawnVehicle(playerOrPos: AltServer.Player | AltServer.Vector3, vehicle: string | number, into?: boolean) {

        if (playerOrPos instanceof AltServer.Player && playerOrPos.vehicle) {
            return;
        }

        const position = playerOrPos instanceof AltServer.Player ? playerOrPos.pos : playerOrPos;
        const rotation = playerOrPos instanceof AltServer.Player ? playerOrPos.rot : new AltServer.Vector3(0, 0, -1.5831648111343384);

        const vehicleObj = new VehicleModel({
            dimension: playerOrPos instanceof AltServer.Player ? playerOrPos.dimension : 0,
            modelVehicle: vehicle,
            position: position,
            rotation: rotation
        });
        VehicleModel.vehiclesList.push(vehicleObj);
        vehicleObj.spawn();
        DataBaseService.instance.insert(vehicleObj, VehicleModel);
        if (into && playerOrPos instanceof AltServer.Player) {
            AltService.emitClient(playerOrPos, EventServerToClient.PED_INTO_VEHICLE, vehicleObj.vehicleAlt);
        }
    }

    private savePlayerPosition(player: AltServer.Player): void {

        let result: Array<{
            pos: AltServer.Vector3,
            rot: AltServer.Vector3
        }> = [];

        if (FileSystem.existsSync(EventService.COORDS_FILE)) {
            result = JSON.parse(FileSystem.readFileSync(EventService.COORDS_FILE).toString());
        }

        AltServer.log(player.pos, player.rot);

        result.push({
            pos: player.pos,
            rot: player.rot
        });

        FileSystem.writeFileSync(EventService.COORDS_FILE, JSON.stringify(result));
    }

    private giveWeapon(player: AltServer.Player, weaponHash: number) {
        player.giveWeapon(weaponHash, 500, true);
    }
    private createObject(model: number, pos: AltServer.Vector3) {
        const obj = new ObjectModel();
        obj.model = model;
        obj.position = pos;
        DataBaseService.instance.insert(obj, ObjectModel);
    }
}