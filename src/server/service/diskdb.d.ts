
declare type QuerySearch = {
    [key: string]: any;
    _id?: string;
};

declare type DataUpdate = {
    [key: string]: any
};

declare type QueryOptions = {
    /**
     * Multiple update ?
     */
    multi: boolean,
    /**
     * if object is not found, add it (update-insert)
     */
    upsert: boolean
};

declare module "diskdb" {

    function connect(path: string, collections?: Array<string>): void;

    function loadCollections(collections: Array<string>): void;

    class UpdateResult {
        updated: number;
        inserted: number;
    }

    class CollectionDb {

        constructor(db: string, collectionName: string);

        collectionName: string;
        find<T>(query?: QuerySearch): Array<T>;
        findOne<T>(query?: QuerySearch): T;
        /**
         * @param data Can be array
         */
        save<T>(data: T): T;
        update<T>(query: QuerySearch, data: T | DataUpdate, options?: QueryOptions): UpdateResult;
        remove(query: QuerySearch, multi?: boolean): boolean;
        count(): number;

    }
}

