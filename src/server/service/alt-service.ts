import * as AltServer from 'alt-server';
import { EventClientToServer, EventServerToClient } from '../../constants/global/events';

export class AltService {

    static onClient(id: EventClientToServer, listener: (...args: any[]) => void) {
        AltServer.onClient(`${id}`, listener);
    }

    static emitClient(player: AltServer.Player, id: EventServerToClient, ...args: any[]) {
        AltServer.emitClient(player, `${id}`, ...args);
    }

}