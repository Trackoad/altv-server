import { Vector3 } from "alt-server";

export class UtilsService {
    static isSame(a: Vector3, b: Vector3, round = 3): boolean {
        return a && b && a.x.toFixed(round) === b.x.toFixed(round) && a.y.toFixed(round) === b.y.toFixed(round) && a.z.toFixed(round) === b.z.toFixed(round);
    }
}