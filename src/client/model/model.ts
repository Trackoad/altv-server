import * as AltClient from 'alt-client';
import * as Native from 'natives';

export class Model {
    static getEntityFrontPosition(entityHandle: number): AltClient.Vector3 {
        let modelDimensions = Native.getModelDimensions(Native.getEntityModel(entityHandle), undefined, undefined);
        return this.getOffsetPositionInWorldCoords(entityHandle, new AltClient.Vector3(0, modelDimensions[2].y, 0));
    }

    static getEntityRearPosition(entityHandle: number, offset = 0): AltClient.Vector3 {
        let modelDimensions = Native.getModelDimensions(Native.getEntityModel(entityHandle), undefined, undefined);
        const dim = modelDimensions[1].y;
        return this.getOffsetPositionInWorldCoords(entityHandle, new AltClient.Vector3(0, modelDimensions[1].y + (this.signNumber(dim) * offset), 0));
    }

    static getEntityRange(entity: AltClient.Vehicle | number, type: "rear" | "front", offsety = 0, offsetz = 0): { p1: AltClient.Vector3, p2: AltClient.Vector3 } {
        let [_, __, dimVector] = Native.getModelDimensions(entity instanceof AltClient.Vehicle ? entity.model : Native.getEntityModel(entity), undefined, undefined);
        const dimY = dimVector.y * (type === "rear" ? -1 : 1);
        const dimX = dimVector.x * (type === "rear" ? -1 : 1);
        const dimZ = dimVector.z;
        return {
            p1: this.getOffsetPositionInWorldCoords(entity instanceof AltClient.Vehicle ? entity.scriptID : entity, new AltClient.Vector3(dimX, dimY, -offsetz)),
            p2: this.getOffsetPositionInWorldCoords(entity instanceof AltClient.Vehicle ? entity.scriptID : entity, new AltClient.Vector3(-dimX, dimY + (this.signNumber(dimY) * offsety), dimZ))
        };
    }

    private static getOffsetPositionInWorldCoords(entityHandle: number, offset: AltClient.Vector3): AltClient.Vector3 {
        return Native.getOffsetFromEntityInWorldCoords(entityHandle, offset.x, offset.y, offset.z);
    }

    private static signNumber(num: number): 1 | -1 {
        return num < 0 ? -1 : 1;
    }
}