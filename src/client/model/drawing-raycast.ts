import * as AltClient from 'alt-client';
import * as Native from 'natives';
import { Render } from './render';

export class RayCastDrawing {

    static active: boolean;

    constructor() {
        Render.render(this.render, this);
    }

    render() {
        if (!RayCastDrawing.active) {
            return;
        }
        let pos = AltClient.Player.local.pos;
        let fv = Native.getEntityForwardVector(AltClient.Player.local.scriptID);
        const boost: AltClient.Vector3 = { x: 100, y: 100, z: 1 };

        for (let i = 1; i < 50; i++) {
            let posFront = {
                x: pos.x + fv.x * boost.x,
                y: pos.y + fv.y * boost.y,
                z: pos.z - i * boost.z
            };
            Native.drawLine(pos.x, pos.y, pos.z, posFront.x, posFront.y, posFront.z, 255, 255, 255, 230);
        }
    }
}