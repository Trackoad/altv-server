// origin : https://github.com/Stuyk/altV-Open-Roleplay-altLife-Official/blob/master/resources/orp/client/utility/raycast.js

import * as AltClient from 'alt-client';
import * as Native from 'natives';

const getFlagNumberByType = <T extends Function>(type: T): number => {
    if (type.prototype === AltClient.Vehicle.prototype) {
        return 2;
    }

    if (type.prototype === AltClient.Player.prototype) {
        return 4;
    }

    if (type.prototype === Number.prototype) {
        return 16;
    }

    if (type.prototype === AltClient.Vector3.prototype) {
        return 1;
    }
}


export function getEntityFromRaycast<T extends Function>(type: T, ignorePlayer = true, boost: AltClient.Vector3 = { x: 7, y: 7, z: 0.1 }): any {

    let pos = AltClient.Player.local.pos;
    let fv = Native.getEntityForwardVector(AltClient.Player.local.scriptID);

    const flagNumber = getFlagNumberByType(type);

    // Cast multiple raycasts from center-forward to center-downards.
    // Think of a pool noodle pointing down.
    for (let i = 1; i < 5; i++) {
        let posFront = {
            x: pos.x + fv.x * boost.x,
            y: pos.y + fv.y * boost.y,
            z: pos.z - i * boost.z
        };

        // Do a ray cast.
        let ray;
        if (ignorePlayer) {
            ray = Native.startShapeTestRay(
                pos.x,
                pos.y,
                pos.z,
                posFront.x,
                posFront.y,
                posFront.z,
                flagNumber,
                AltClient.Player.local.scriptID,
                0
            );
        } else {
            ray = Native.startShapeTestRay(
                pos.x,
                pos.y,
                pos.z,
                posFront.x,
                posFront.y,
                posFront.z,
                flagNumber,
                undefined,
                0
            );
        }

        // Get the Result
        // eslint-disable-next-line no-unused-vars
        let [_, _hit, _endCoords, _surfaceNormal, _entity] = Native.getShapeTestResult(ray, false, null, null, 1);

        // Check if the entity was hit.
        if (_hit) {
            // Vehicle Type
            if (type.prototype === AltClient.Vehicle.prototype) {
                return <T><any>AltClient.Vehicle.all.find(v => v.scriptID === _entity);
            }

            // Player Type
            if (type.prototype === AltClient.Player.prototype) {
                return <T><any>_entity;
            }

            // Return the entityNumber for object.
            if (type.prototype === Number.prototype) {
                return <T><any>_entity;
            }

            // Just the map
            if (type.prototype === AltClient.Vector3.prototype) {
                return <T><any>_endCoords;
            }
        }
    }

    return undefined;
}