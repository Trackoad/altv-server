import { RenderFunction } from '../../constants/client/render-functions';
import { AltService } from '../service/alt-service';
import * as AltClient from 'alt-client';
import * as Native from 'natives';

type CallBack = () => void;

export class Render {

    static render(callback: CallBack, element: any) {
        AltClient.everyTick(callback.bind(element));
    }

}