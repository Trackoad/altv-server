import { EventService } from './service/event-service';
import { CommandService } from './service/console-service';

class Client {

    static start(_args?: Array<string>) {

        new EventService();

        new CommandService();

    }
}

Client.start();