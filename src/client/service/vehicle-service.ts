import * as AltClient from 'alt-client';
import * as Native from 'natives';
import { DoorEntity, DoorEntityDetail } from '../../constants/client/vehicle-doors';
import { MetaData } from '../../constants/global/meta-data';

export class VehicleService {

    private static timer = 2000;

    /**
     * La porte est elle ouverte ?
     */
    static isDoorOpen(vehicle: AltClient.Vehicle, door: DoorEntityDetail | DoorEntity): boolean {
        const meta = vehicle.getMeta(MetaData.vehicle.DOOR_STATE);
        if (meta) {
            return meta[door.index];
        }
    }

    /**
     * Sans intervention du joueur
     */
    static openCloseVehicleDoor(vehicle: AltClient.Vehicle, door: DoorEntityDetail | DoorEntity, autoClose = true, canPlayerOpen = true): Promise<void> {
        let meta = vehicle.getMeta(MetaData.vehicle.DOOR_STATE);
        if (!meta) {
            meta = {}
        }
        if (canPlayerOpen && ((door instanceof DoorEntityDetail && door.canOpen) || door instanceof DoorEntity)) {
            return new Promise((resolve, reject) => {
                Native.taskOpenVehicleDoor(AltClient.Player.local.scriptID, vehicle.scriptID, this.timer, door.index, this.timer);
                AltClient.setTimeout(resolve, this.timer);
            })
        }
        if (!this.isDoorOpen(vehicle, door)) {
            Native.setVehicleDoorOpen(vehicle.scriptID, door.index, false, false);
            meta[door.index] = true;
        } else if (autoClose) {
            Native.setVehicleDoorShut(vehicle.scriptID, door.index, false);
            meta[door.index] = false;
        }
        vehicle.setMeta(MetaData.vehicle.DOOR_STATE, meta);
        return Promise.resolve();
    }
}