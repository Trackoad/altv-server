import * as AltClient from 'alt-client';
import { EventClientToServer, EventServerToClient } from '../../constants/global/events';

export class AltService {

    static emitServer(id: EventClientToServer, ...args: any[]) {
        AltClient.emitServer(`${id}`, ...args);
    }

    static onServer(id: EventServerToClient, listener: (...args: any[]) => void) {
        AltClient.onServer(`${id}`, listener);
    }

    static log(...args: any[]) {
        AltService.emitServer(EventClientToServer.LOG, ...args);
    }

    static operationVector(v1: AltClient.Vector3, v2: AltClient.Vector3, operation: '-' | '+' | 'x' | '/'): AltClient.Vector3 {
        let vectorResult = { x: 0, y: 0, z: 0 };
        switch (operation) {
            case '+':
                vectorResult.x = v1.x + v2.x;
                vectorResult.y = v1.y + v2.y;
                vectorResult.z = v1.z + v2.z;
                break;
            case '-':
                vectorResult.x = v1.x - v2.x;
                vectorResult.y = v1.y - v2.y;
                vectorResult.z = v1.z - v2.z;
                break;
            case 'x':
                vectorResult.x = v1.x * v2.x;
                vectorResult.y = v1.y * v2.y;
                vectorResult.z = v1.z * v2.z;
                break;
            case '/':
                vectorResult.x = v1.x / (!v2.x ? 1 : v2.x);
                vectorResult.y = v1.y / (!v2.y ? 1 : v2.y);
                vectorResult.z = v1.z / (!v2.z ? 1 : v2.z);
                break;

        }
        return vectorResult;
    }
}