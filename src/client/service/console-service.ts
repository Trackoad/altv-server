import * as AltClient from 'alt-client';
import { EventClientToServer } from '../../constants/global/events';
import { VehiclesMapped } from '../../constants/global/vehicles';
import { AltService } from './alt-service';

export class CommandService {

    constructor() {
        AltClient.on("consoleCommand", (name: string, ...args: Array<any>) => {
            switch (name) {
                case "veh":
                    this.spawnVehicle(args[0]);
                    break;

                default:
                    AltClient.log("Command not find", name)
                    break;
            }
        });
    }

    private spawnVehicle(vehicle: string) {

        const hashVehicle = VehiclesMapped[vehicle];
        if (hashVehicle) {
            AltService.emitServer(EventClientToServer.SPAWN_VEHICLE, hashVehicle);
        } else {
            AltClient.log("Vehicle not find", vehicle);
        }
    }
}
