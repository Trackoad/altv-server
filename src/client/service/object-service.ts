import * as AltClient from 'alt-client';
import * as Native from 'natives';
import { MenuService } from './menu-service';
import { AltService } from './alt-service';
import { EntityObject } from '../../constants/client/objects';
import { UpdateObjectType, ObjectCreate } from '../../constants/global/events';

type Zone = number;

type EntityObjectModelConstructor = {
    id: string,
    pos: AltClient.Vector3,
    model: number
};

class EntityObjectModel {

    idGlobal: string;

    idLocal: number;

    model: EntityObject;

    pos: AltClient.Vector3;

    private _mbCreated: boolean;

    private _mbDestroyed: boolean;

    constructor(param: EntityObjectModelConstructor) {
        this.pos = param.pos;
        this.model = param.model;
        this.idGlobal = param.id;
    }

    create() {
        if (!this._mbCreated) {
            this._mbCreated = true;
            this._mbDestroyed = false;
            this.idLocal = Native.createObject(this.model, this.pos.x, this.pos.y, this.pos.z, false, false, false);
            Native.setEntityCanBeDamaged(this.idLocal, false);
            Native.setObjectPhysicsParams(this.idLocal, 5000, 1, 1, 1, 1, 10, 1, 1, 1, 1, 1);
        }
    }

    destroy() {
        if (!this._mbDestroyed) {
            this._mbDestroyed = true;
            this._mbCreated = false;
            Native.deleteObject(this.idLocal);
        }
    }

    isActive(): boolean {
        return this._mbCreated && !this._mbDestroyed;
    }
}

export class ObjectService {

    private static MAX_SPEED_TO_SPAWN_OBJECT = 30;

    private static RANGE_DIAMETER_PLAYER = 300;

    private _currentZone: Zone;

    private _objectByZone: Map<Zone, Array<EntityObjectModel>> = new Map();

    private _generatedByZone: Map<Zone, boolean> = new Map();

    private _zonesAround: Array<Zone> = [];

    private _lastUpdate = Date.now();

    private get localPlayer(): AltClient.Player {
        return AltClient.Player.local;
    }

    private static _instance: ObjectService;
    static get instance(): ObjectService {
        return this._instance ? this._instance : this._instance = new ObjectService();
    }

    raycast() {
        if (AltClient.Player.local.vehicle) {
            return;
        }
        MenuService.instance.openUIVehicle() ||
            MenuService.instance.openUIObject();
    }

    renderObject() {

        Native.setPlayerHealthRechargeMultiplier(this.localPlayer.model, 10000);

        const playerZone = Native.getZoneAtCoords(this.localPlayer.pos.x, this.localPlayer.pos.y, this.localPlayer.pos.z);
        if (playerZone === this._currentZone && this.isGenerated(this._currentZone)) {
            return;
        }
        const zonesAround = this.getAllZonesInArea();
        this.destroyObjects(this._zonesAround.filter(zone => !zonesAround.includes(zone)));
        this._zonesAround = zonesAround;
        this._currentZone = playerZone;
        // if (this.isPlayerTooFast()) {
        //     return;
        // }
        this.createObjects(zonesAround);
    }

    updateObjects(objects: UpdateObjectType) {
        this._lastUpdate = Date.now();

        objects.forEach(element => {
            let object: EntityObjectModel;
            switch (element.key) {
                case 'U':

                    break;
                case 'C':
                    const value = (<ObjectCreate>element.value);
                    this.addObject(value.zone, object = new EntityObjectModel({
                        id: value.id,
                        model: value.model,
                        pos: value.pos
                    }));
                    break;
                case 'D':

                    break;
                default:
                    throw new Error(`bad type ${element.key}`)
            }
        })
    }

    private isPlayerTooFast(): boolean {
        const velocityPlayer = Native.getEntityVelocity(this.localPlayer.scriptID);
        const vitesse = Math.abs(velocityPlayer.x) + Math.abs(velocityPlayer.y) + Math.abs(velocityPlayer.z);
        return vitesse > ObjectService.MAX_SPEED_TO_SPAWN_OBJECT;
    }

    private destroyObjects(zones: Array<Zone>) {
        zones.filter(zone => zone).forEach(zone => {

            const objects = (this._objectByZone.get(zone) || []);
            if (objects.length) {
                AltService.log('destroy', objects.length);
            }
            objects.forEach(object => object.destroy());
            this._generatedByZone.delete(zone);
        })
    }

    private createObjects(zones: Array<Zone>) {
        zones.forEach((zone, _, a) => {
            const objects = (this._objectByZone.get(zone) || []);
            if (objects.length) {
                AltService.log('create', objects.length);
            }
            objects.forEach(object => object.create());
            this._generatedByZone.delete(zone);
            this._generatedByZone.set(zone, true);
        })
    }

    private isGenerated(zone: Zone) {
        if (!this._generatedByZone.has(zone)) {
            this._generatedByZone.set(zone, false);
        }
        return this._generatedByZone.get(zone);
    }

    private getAllZonesInArea(): Array<Zone> {
        const result: Array<Zone> = [];
        let i = 0;
        const radiusRange = ObjectService.RANGE_DIAMETER_PLAYER / 2;
        for (let radius = radiusRange < 25 ? 0 : 25; radius <= radiusRange; radius += 25) {
            const possibleValues = [-radius, -7 / radius * radius, 0, radius, 7 / radius * radius];
            for (const x of possibleValues) {
                for (const y of possibleValues) {
                    const testVector = AltService.operationVector(this.localPlayer.pos, new AltClient.Vector3(x, y, 0), '+');
                    const testZone = Native.getZoneAtCoords(testVector.x, testVector.y, 0);
                    if (typeof testZone === 'number' && !result.includes(testZone)) {
                        result.push(testZone);
                    }
                }
            }
        }
        return result;
    }

    private addObject(zone: Zone, object: EntityObjectModel) {
        if (!this._objectByZone.has(zone)) {
            this._objectByZone.set(zone, []);
        }
        this._objectByZone.get(zone).push(object);
    }
}