import * as AltClient from 'alt-client';
import * as Native from 'natives';
import { EventServerToClient, EventClientToServer } from '../../constants/global/events';
import { AltService } from './alt-service';
import { KeyCode } from '../../constants/client/key-code';
import { MenuService } from './menu-service';
import { ObjectService } from './object-service';
import { EntityObject } from '../../constants/client/objects';
import { Render } from '../model/render';
import { RenderFunction } from '../../constants/client/render-functions';
import { RayCastDrawing } from '../model/drawing-raycast';

export class EventService {

    private static TIMER_RENDER_OBJECT = 300;

    constructor() {

        new RayCastDrawing();

        this.loadingClient()
            .then(() => {

                AltClient.on("keyup", key => {
                    AltClient.setTimeout(() => {
                        switch (key) {
                            case KeyCode.BACKQUOTE:
                                MenuService.instance.openUIDev();
                                break;
                            case KeyCode.TWO_POINT:
                                ObjectService.instance.raycast();
                                break;
                            case KeyCode.SLASH:

                                RayCastDrawing.active = !RayCastDrawing.active;

                                // Render.instance.addFunction(RenderFunction.MELON, this.render, undefined);
                                break;
                            default:
                                break;
                        }

                    }, 100);
                });

                AltClient.setInterval(() => ObjectService.instance.renderObject(), EventService.TIMER_RENDER_OBJECT);

                AltClient.on('anyResourceError', (ressource) => AltService.emitServer(EventClientToServer.ERROR, ressource))

                AltService.onServer(EventServerToClient.PED_INTO_VEHICLE, vehicle => this.setPedIntoVehicle(vehicle));
                AltService.onServer(EventServerToClient.DISABLE_CONTROL, state => this.disableControls(state));
                AltService.onServer(EventServerToClient.UPDATE_OBJECTS, objets => ObjectService.instance.updateObjects(objets));
            })

    }

    private render() {
        const posA = { x: -1138.83, y: -713.473, z: 20.9729 };
        const posB = { x: -1142.23, y: -725.341, z: 22.3715 };
        Native.drawBox(posA.x, posA.y, posA.z, posB.x, posB.y, posB.z, 10, 10, 10, 255);
    }

    private setPedIntoVehicle(vehicle: AltClient.Vehicle) {
        AltClient.setTimeout(() => {
            Native.setPedIntoVehicle(AltClient.Player.local.scriptID, vehicle.scriptID, -1);
        }, 500);
    }

    private disableControls(state: boolean) {
        AltClient.toggleGameControls(state)
    }

    private loadingClient(): Promise<any> {
        return new Promise((resolve) => {
            AltService.onServer(EventServerToClient.UPDATE_OBJECTS, objets => {
                ObjectService.instance.updateObjects(objets);
                resolve();
            })
        });
    }
}