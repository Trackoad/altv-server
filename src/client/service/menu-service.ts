import * as AltClient from 'alt-client';
import NativeUI, { Point, UIMenuItem } from "../NativeUI/NativeUi";
import { getEntityFromRaycast } from "../model/raycast";
import { VehicleService } from './vehicle-service';
import { CHEST_DOORS, DEFAULT_CHEST, DoorEntity, DEFAULT_CHEST_MORE_THAN, DoorEntityDetail } from '../../constants/client/vehicle-doors';
import * as Native from 'natives';
import { Model } from '../model/model';
import { VehiclesMapped } from '../../constants/global/vehicles';
import { EventMenu } from '../../constants/client/menu-events';
import { WeaponsMapped } from '../../constants/global/weapons';
import { AltService } from './alt-service';
import { EventClientToServer } from '../../constants/global/events';
import { EntityObject } from '../../constants/client/objects';

export class MenuService {

    private static _instance: MenuService;
    static get instance(): MenuService {
        return this._instance ? this._instance : this._instance = new MenuService();
    }

    private uiDev: NativeUI = new NativeUI("Devsss", "", new Point(50, 50));

    private uiVehicle: NativeUI = new NativeUI("Vehicule", "", new Point(50, 50));

    private uiObject: NativeUI = new NativeUI("Objets", "", new Point(50, 50));

    private openChest: UIMenuItem = new UIMenuItem("Ouvrir / Fermer coffre", "", 42);

    private entityNUmber: number;

    private constructor() {
        this.initUIVehicle();
        this.initUIObject();
        this.initUIDev();
    }

    private initUIVehicle() {

        this.uiVehicle.AddItem(this.openChest);

        this.uiVehicle.ItemSelect.on((element: UIMenuItem) => {

            const vehicle: AltClient.Vehicle = this.openChest.Data.more;

            if (element.Data.eventName === 42) {
                let promise = Promise.resolve();
                const doorsCount = Native.getNumberOfVehicleDoors(vehicle.scriptID);
                (<Array<DoorEntity | DoorEntityDetail>>(CHEST_DOORS.get(vehicle.model)) || (doorsCount > 4 ? DEFAULT_CHEST_MORE_THAN : DEFAULT_CHEST))
                    .forEach(door => promise = promise.then(() => VehicleService.openCloseVehicleDoor(vehicle, door)))
            }

            this.uiVehicle.Close(true);
        })
    }

    private initUIObject() {
        this.uiObject.AddItem(new UIMenuItem("Test", "", 42));
        this.uiObject.AddItem(new UIMenuItem("Model", "Get model of entity", 43));

        this.uiObject.ItemSelect.on((element: UIMenuItem) => {

            if (element.Data.eventName === 42) {

                const pos = Model.getEntityRange(this.entityNUmber, "rear", 2, 1);

                Native.createObject(EntityObject.OLD_SCREEN_COMPUTER, pos.p1.x, pos.p1.y, pos.p1.z, false, true, false)
                Native.createObject(EntityObject.OLD_SCREEN_COMPUTER, pos.p2.x, pos.p2.y, pos.p2.z, false, true, false)

            }

            if (element.Data.eventName === 43) {
                AltService.log("Entity model : ", Native.getEntityModel(this.entityNUmber));
            }

            this.uiObject.Close(true);
        });
    }

    private initUIDev() {
        const menuVehicles = new NativeUI("Vehicles", "", new Point(50, 50));

        let oMap = new Map();
        Object.keys(VehiclesMapped).forEach(vehicleName => oMap.set(vehicleName, VehiclesMapped[vehicleName]))
        oMap.forEach((hash, vehicleName) => {
            menuVehicles.AddItem(new UIMenuItem(vehicleName, "", {
                eventName: -1,
                more: hash
            }));
        });
        this.uiDev.AddSubMenu(menuVehicles, new UIMenuItem("Spawn", "", null));
        menuVehicles.ItemSelect.on((element: UIMenuItem) => AltService.emitServer(EventClientToServer.SPAWN_VEHICLE, element.Data.more));

        const menuWeapons = new NativeUI("Weapons", "", new Point(50, 50));
        oMap = new Map();
        Object.keys(WeaponsMapped).forEach(category => {
            const subCategoryMenu = new NativeUI(category, "", new Point(50, 50));
            const oCat = WeaponsMapped[category];
            Object.keys(oCat).forEach(weapon => {
                subCategoryMenu.AddItem(new UIMenuItem(weapon, "", {
                    eventName: -1,
                    more: oCat[weapon]
                }))
            })
            menuWeapons.AddSubMenu(subCategoryMenu, new UIMenuItem(category, "", null));
            subCategoryMenu.ItemSelect.on((element: UIMenuItem) => AltService.emitServer(EventClientToServer.GIVE_WEAPON, element.Data.more));
        });
        this.uiDev.AddSubMenu(menuWeapons, new UIMenuItem("Weapons", "", null));

        this.uiDev.AddItem(new UIMenuItem("Object", "", EventMenu.SPAWN_OBJECT));
        this.uiDev.AddItem(new UIMenuItem("Kick", "", EventMenu.KICK));
        this.uiDev.AddItem(new UIMenuItem("Coords", "", EventMenu.COORD));
        this.uiDev.AddItem(new UIMenuItem("Count doors", "", EventMenu.DOORS_COUNT));
        this.uiDev.AddItem(new UIMenuItem("Respawn", "", EventMenu.REVIVE));
        this.uiDev.ItemSelect.on((element: UIMenuItem) => {

            if (!element.Data) {
                return;
            }

            if (element.Data.eventName === EventMenu.SPAWN_OBJECT) {

                const pos = AltService.operationVector(AltClient.Player.local.pos, new AltClient.Vector3(1, 0, -1), "+");
                AltService.emitServer(EventClientToServer.CREATE_OBJECT, EntityObject.BAREL, pos)

            }

            if (element.Data.eventName === EventMenu.KICK) {
                AltService.emitServer(EventClientToServer.KICK_PLAYER);
            }

            if (element.Data.eventName === EventMenu.COORD) {
                AltService.emitServer(EventClientToServer.COORD);
            }

            if (element.Data.eventName === EventMenu.DOORS_COUNT) {
                if (AltClient.Player.local.vehicle) {
                    AltService.emitServer(EventClientToServer.LOG, Native.getNumberOfVehicleDoors(AltClient.Player.local.vehicle.scriptID));
                }
            }

            if (element.Data.eventName === EventMenu.REVIVE) {
                AltService.emitServer(EventClientToServer.REVIVE);
            }

            this.uiDev.Close();
        });
    }

    openUIVehicle(): boolean {
        this.openChest.Data.more = getEntityFromRaycast(AltClient.Vehicle);
        if (!this.openChest.Data.more) {
            return;
        }

        const pos = Model.getEntityRange(this.openChest.Data.more, "rear", 2, 1);
        this.uiVehicle.RemoveItem(this.openChest);
        if (Native.getNumberOfVehicleDoors(this.openChest.Data.more.scriptID) > 3 && Native.isEntityInArea(AltClient.Player.local.scriptID, pos.p1.x, pos.p1.y, pos.p1.z, pos.p2.x, pos.p2.y, pos.p2.z, false, false, undefined)) {
            this.uiVehicle.AddItem(this.openChest);
        }

        if (!this.uiVehicle.containsItems()) {
            return;
        }

        this.uiVehicle.Open();
        return true;
    }

    openUIObject(): boolean {
        this.entityNUmber = getEntityFromRaycast(Number, true, { x: 7, y: 7, z: 2 });
        if (!this.entityNUmber) {
            return;
        }

        this.uiObject.Open();
        return true;
    }

    openUIDev() {
        this.uiDev.Open();
    }
}