export enum EventClientToServer {
    KICK_PLAYER,
    SPAWN_VEHICLE,
    COORD,
    GIVE_WEAPON,
    LOG,
    ERROR,
    WARNING,
    REVIVE,
    CREATE_OBJECT,
}

export enum EventServerToClient {
    PED_INTO_VEHICLE,
    DISABLE_CONTROL,
    UPDATE_OBJECTS,
}

export type UpdateObjectState = 'U' | 'C' | 'D';

export type VectorPosition = { x: number, y: number, z: number };

export type ObjectUpdate = { pos: VectorPosition, id: string };

export type ObjectCreate = { pos: VectorPosition, model: number, id: string, zone: number };

export type ObjectDelete = number;

export type UpdateObjectType = Array<{ key: UpdateObjectState, value: ObjectCreate | ObjectDelete | ObjectUpdate }>;