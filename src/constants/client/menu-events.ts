export enum EventMenu {
    KICK,
    COORD,
    DOORS_COUNT,
    REVIVE,
    SPAWN_OBJECT,
}