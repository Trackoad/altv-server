export const KeyCode = {
    /**
     * ²
     */
    BACKQUOTE: 222,
    /**
     * !
     */
    SLASH: 223,
    /**
     * :
     */
    TWO_POINT: 188
}