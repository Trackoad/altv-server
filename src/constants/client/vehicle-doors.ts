import { Vehicles } from "../global/vehicles";

export class DoorEntity {

    static DOORS = {
        // Front Left Door
        F_L: new DoorEntity(0),
        // Front Right Door
        F_R: new DoorEntity(1),
        // Back Left Door
        B_L: new DoorEntity(2),
        // Back Right Door
        B_R: new DoorEntity(3),
        // Hood
        HOOD: new DoorEntity(4),
        // Trunk
        COFFRE: new DoorEntity(5),
        // Back
        B1: new DoorEntity(6),
        // Back2
        B2: new DoorEntity(7),
    }

    private _index: number;
    get index(): number {
        return this._index;
    }

    private constructor(index: number) {
        this._index = index;
    }
}

export class DoorEntityDetail {
    door: DoorEntity;
    canOpen: boolean;

    get index(): number {
        return this.door ? this.door.index : undefined;
    }

    constructor(door: DoorEntity, canOpen?: boolean) {
        this.door = door;
        this.canOpen = canOpen;
    }
}

export const CHEST_DOORS: Map<number, Array<DoorEntity>> = new Map([
    [Vehicles.burrito, [DoorEntity.DOORS.F_R, DoorEntity.DOORS.B_L]],
]);

export const DEFAULT_CHEST = [new DoorEntityDetail(DoorEntity.DOORS.COFFRE), new DoorEntityDetail(DoorEntity.DOORS.B_L), new DoorEntityDetail(DoorEntity.DOORS.B_R)];

export const DEFAULT_CHEST_MORE_THAN = [new DoorEntityDetail(DoorEntity.DOORS.COFFRE)];